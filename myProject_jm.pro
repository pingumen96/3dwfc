TEMPLATE = app
TARGET = myProject
DEPENDPATH += .
INCLUDEPATH += .
INCLUDEPATH += ./src
QT += opengl xml gui
CONFIG += qt \
    release \
    debug \
    c++11
MOC_DIR = ./tmp/moc
OBJECTS_DIR = ./tmp/obj

# Input
HEADERS += ./src/point3.h \
    ./src/WFCMesh.h \
    ./src/MyViewer.h \
    ./src/gl/GLUtilityMethods.h \
    ./src/gl/BasicColors.h \
    src/BasicIO.h \
    src/Texture.h \
    src/WFCFace.h \
    src/WFCPattern.h \
    src/gl/openglincludeQtComp.h \
    src/gl/stb_image.h
SOURCES += ./src/main.cpp \
    ./src/gl/GLUtilityMethods.cpp\
    ./src/gl/BasicColors.cpp \
    src/Texture.cpp \
    src/WFCFace.cpp \
    src/WFCMesh.cpp \
    src/WFCPattern.cpp



EXT_DIR = ../extern_jm


# cg3lib
CONFIG += CG3_CORE CG3_MESHES

include (extern/cg3.pri)


#QGLViewer
{
 INCLUDEPATH += $${EXT_DIR}/libQGLViewer-2.6.1
 LIBS +=    -L$${EXT_DIR}/libQGLViewer-2.6.1/QGLViewer \
            -lQGLViewer
}

#glm:
{
 INCLUDEPATH += $${EXT_DIR}/glm-0.9.9.5/glm
}


LIBS += -lglut \
    -lGLU
LIBS += -lgsl \
    -lgomp
LIBS += -lblas \
    -lgomp
release:QMAKE_CXXFLAGS_RELEASE += -O3 \
    -fopenmp
release:QMAKE_CFLAGS_RELEASE += -O3 \
    -fopenmp
