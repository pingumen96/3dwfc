#ifndef WFCFACE_H
#define WFCFACE_H
#include <memory>
#include <unordered_map>
#include <cg3/meshes/dcel/dcel.h>
#include "Texture.h"
#include "WFCPattern.h"

using namespace std;

class WFCFace {
public:
    WFCFace();
    WFCFace(shared_ptr<WFCPattern> assignedPattern, unsigned int correspondingFaceIndex, const vector<shared_ptr<WFCPattern>>& defaultPatterns);

    void setPattern(shared_ptr<WFCPattern> patternPointer, short numRotations = 0) { isSet = true; assignedPattern = patternPointer; this->numRotations = numRotations % 3; }
    bool isAssigned() const;
    inline short getNumRotations() const {return numRotations;}
    const shared_ptr<WFCPattern>& getPattern() const {return assignedPattern;}
    const unordered_map<shared_ptr<WFCPattern>, vector<unsigned short>>& getPossiblePatterns() { return possiblePatterns; }
    unsigned int getCorrespondingFaceIndex() {return correspondingFaceIndex;}
    pair<cg3::Dcel::Face*, unsigned short> getNeighbourAtEdge(cg3::Dcel::Face* face, unsigned short edgeIndex);
    void removePossiblePatternRotation(shared_ptr<WFCPattern> patternToRemove, unsigned short rotation);
    void addPatternPossibleRotation(shared_ptr<WFCPattern> possiblePattern, unsigned short rotation);
private:
    bool isSet;
    shared_ptr<WFCPattern> assignedPattern;
    short numRotations;
    unordered_map<shared_ptr<WFCPattern>, vector<unsigned short>> possiblePatterns;
    unsigned int correspondingFaceIndex;
};

#endif // WFCFACE_H
