#include "WFCFace.h"




WFCFace::WFCFace() {
}

WFCFace::WFCFace(shared_ptr<WFCPattern> assignedPattern, unsigned int correspondingFaceIndex, const vector<shared_ptr<WFCPattern>>& defaultPatterns)
    : isSet(false)
    , assignedPattern(assignedPattern)
    , numRotations(0)
    , correspondingFaceIndex(correspondingFaceIndex) {
    for(unsigned int i = 0; i < defaultPatterns.size(); i++) {
        possiblePatterns[defaultPatterns[i]] = vector<unsigned short>();
    }
}

bool WFCFace::isAssigned() const {
    return isSet;
}

pair<cg3::Dcel::Face*, unsigned short> WFCFace::getNeighbourAtEdge(cg3::Dcel::Face* face, unsigned short edgeIndex) {
    unsigned short counter = 0;
    cg3::Dcel::Face* neighbourFace = nullptr;
    auto he = *(face->incidentHalfEdgeIterator().begin());

    for(int i = 0; i < 3; i++) {
        if(counter == edgeIndex) {
            neighbourFace = he->twin()->face();
            break;
        }
        counter++;
        he = he->next();
    }

    int secondCounter = 0;
    auto he2 = *(neighbourFace->incidentHalfEdgeBegin());
    for(int i = 0; i < 3; i++) {
        if(he2->twin()->face()->id() == face->id()) {
            break;
        }
        he2 = he2->next();
        secondCounter++;
    }

    return make_pair(neighbourFace, secondCounter);
}

void WFCFace::removePossiblePatternRotation(shared_ptr<WFCPattern> patternToRemove, unsigned short rotation) {
    for(int i = 0; i < possiblePatterns[patternToRemove].size(); i++) {
        if(possiblePatterns[patternToRemove][i] == rotation) {
            possiblePatterns[patternToRemove].erase(possiblePatterns[patternToRemove].begin() + i);
            break;
        }
    }
}

void WFCFace::addPatternPossibleRotation(shared_ptr<WFCPattern> possiblePattern, unsigned short rotation) {
    possiblePatterns[possiblePattern].emplace_back(rotation);
}
