#ifndef PROJECTMESH_H
#define PROJECTMESH_H

#include <vector>
#include <tuple>
#include <unordered_map>
#include <map>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <cg3/meshes/dcel/dcel.h>
#include "WFCPattern.h"
#include "WFCFace.h"
#include "Texture.h"

class WFCMesh : public cg3::Dcel {
public:
    WFCMesh();

    inline long getCurrentFaceIndex() { return currentFaceId; }
    const WFCFace& getWFCFace(unsigned int index) {return wfcFaces[index];}

    void initializeWfcFaces();
    void drawFace();

    std::vector<std::shared_ptr<WFCPattern>> const & getPatterns() { return patterns; }
private:
    bool isPatternValid( long faceIdx , std::shared_ptr<WFCPattern> patternPtr , short rotation );
    std::unordered_map<unsigned int, WFCFace> wfcFaces;
    long currentFaceId; // l'id in realtà è l'indice del vector che le contiene
    std::vector<std::shared_ptr<WFCPattern>> patterns;
};



#endif // PROJECTMESH_H
