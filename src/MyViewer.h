#ifndef MYVIEWER_H
#define MYVIEWER_H

// boost
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

// Mesh stuff:
#include "WFCMesh.h"
#include "WFCFace.h"

// Parsing:
#include "BasicIO.h"

// opengl and basic gl utilities:
#define GL_GLEXT_PROTOTYPES
#include <gl/openglincludeQtComp.h>
#include <GL/glext.h>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLFunctions>
#include <QGLViewer/qglviewer.h>

#include <gl/GLUtilityMethods.h>

// Qt stuff:
#include <QFormLayout>
#include <QToolBar>
#include <QColorDialog>
#include <QFileDialog>
#include <QKeyEvent>
#include <QInputDialog>
#include <QLineEdit>


#include "qt/QSmartAction.h"


class MyViewer : public QGLViewer , public QOpenGLFunctions_4_3_Core
{
    Q_OBJECT

    WFCMesh mesh;
    boost::numeric::ublas::matrix<float> uvCoordinates;

    QWidget * controls;

public :

    MyViewer(QGLWidget * parent = nullptr) : QGLViewer(parent) , QOpenGLFunctions_4_3_Core() {
        uvCoordinates = boost::numeric::ublas::matrix<float>(3, 2);
        // clock wise
        //        uvCoordinates(0, 0) = 0.0;
        //        uvCoordinates(0, 1) = 0.0;
        //        uvCoordinates(1, 0) = 0.5;
        //        uvCoordinates(1, 1) = 1.0;
        //        uvCoordinates(2, 0) = 1.0;
        //        uvCoordinates(2, 1) = 0.0;

        // CAREFUL !!!!   YOU NEED TO LOAD MESHES THAT ARE COUNTER-CLOCK-WISE ORIENTED   !!!!!
        // counter clock wise
        uvCoordinates(0, 0) = 0.0;
        uvCoordinates(0, 1) = 0.0;
        uvCoordinates(1, 0) = 1.0;
        uvCoordinates(1, 1) = 0.0;
        uvCoordinates(2, 0) = 0.5;
        uvCoordinates(2, 1) = 1.0;
    }



    void add_actions_to_toolBar(QToolBar *toolBar)
    {
        // Specify the actions :
        DetailedAction * open_mesh = new DetailedAction( QIcon("./icons/open.png") , "Open Mesh" , "Open Mesh" , this , this , SLOT(open_mesh()) );
        DetailedAction * save_mesh = new DetailedAction( QIcon("./icons/save.png") , "Save model" , "Save model" , this , this , SLOT(save_mesh()) );
        DetailedAction * help = new DetailedAction( QIcon("./icons/help.png") , "HELP" , "HELP" , this , this , SLOT(help()) );
        DetailedAction * saveCamera = new DetailedAction( QIcon("./icons/camera.png") , "Save camera" , "Save camera" , this , this , SLOT(saveCamera()) );
        DetailedAction * openCamera = new DetailedAction( QIcon("./icons/open_camera.png") , "Open camera" , "Open camera" , this , this , SLOT(openCamera()) );
        DetailedAction * saveSnapShotPlusPlus = new DetailedAction( QIcon("./icons/save_snapshot.png") , "Save snapshot" , "Save snapshot" , this , this , SLOT(saveSnapShotPlusPlus()) );

        // Add them :
        toolBar->addAction( open_mesh );
        toolBar->addAction( save_mesh );
        toolBar->addAction( help );
        toolBar->addAction( saveCamera );
        toolBar->addAction( openCamera );
        toolBar->addAction( saveSnapShotPlusPlus );
    }

    void animate() {
        QGLViewer::animate();
    }


    void draw() {
        // texture
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        glEnable(GL_DEPTH_TEST);
        glEnable( GL_LIGHTING );
        glColor3f(0.5,0.5,0.8);
        unsigned int i = 0;
        for(auto f : mesh.faceIterator()) {
            glNormal3f(f->normal().x(), f->normal().y(), f->normal().y());
            if(mesh.getWFCFace(i).isAssigned()) {
                mesh.getWFCFace(i).getPattern()->getTexture()->setActive(true);
                glBegin(GL_TRIANGLES);
                int j = 0;
                for(auto e : f->incidentHalfEdgeIterator()) {
                    glTexCoord2f(uvCoordinates((-(int)(mesh.getWFCFace(i).getNumRotations()) + j + 3) % 3, 0),
                                 uvCoordinates((-(int)(mesh.getWFCFace(i).getNumRotations()) + j + 3) % 3, 1));
                    // CAREFUL !!!!  THE FIRST VERTEX IS THE "from" OF THE FIRST EDGE !!!!!
                    glVertex3f(e->fromVertex()->coordinate().x(), e->fromVertex()->coordinate().y(), e->fromVertex()->coordinate().z());
                    j++;
                }
                glEnd();
                mesh.getWFCFace(i).getPattern()->getTexture()->setActive(false);
            } else {
                glBegin(GL_TRIANGLES);
                glVertex3f(f->vertex1()->coordinate().x(), f->vertex1()->coordinate().y(), f->vertex1()->coordinate().z());
                glVertex3f(f->vertex2()->coordinate().x(), f->vertex2()->coordinate().y(), f->vertex2()->coordinate().z());
                glVertex3f(f->vertex3()->coordinate().x(), f->vertex3()->coordinate().y(), f->vertex3()->coordinate().z());
                glEnd();
            }
            i++;
        }
        glEnd();


        glFlush();
        glDisable(GL_TEXTURE_2D);
    }

    void pickBackgroundColor() {
        QColor _bc = QColorDialog::getColor( this->backgroundColor(), this);
        if( _bc.isValid() ) {
            this->setBackgroundColor( _bc );
            this->update();
        }
    }

    void adjustCamera( glm::vec3 const & bb , glm::vec3 const & BB ) {
        glm::vec3 const & center = ( bb + BB )/2.f;
        setSceneCenter( qglviewer::Vec( center.x , center.y , center.z ) );
        setSceneRadius( 1.5f * ( BB - bb ).length() );
        showEntireScene();
    }


    void init() {
        makeCurrent();
        initializeOpenGLFunctions();

        setMouseTracking(true);// Needed for MouseGrabber.
        setAnimationPeriod(0);
        startAnimation();

        setBackgroundColor(QColor(255,255,255));

        // Lights:
        GLTools::initLights();
        GLTools::setSunsetLight();
        GLTools::setDefaultMaterial();

        //
        glShadeModel(GL_SMOOTH);
        glFrontFace(GL_CCW); // CCW ou CW

        glEnable(GL_DEPTH);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_CLIP_PLANE0);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_COLOR_MATERIAL);

        //
        setSceneCenter( qglviewer::Vec( 0 , 0 , 0 ) );
        setSceneRadius(10.f);
        showEntireScene();
    }

    QString helpString() const {
        QString text("<h2>Our cool project</h2>");
        text += "<p>";
        text += "This is a research application, it can explode.";
        text += "<h3>Participants</h3>";
        text += "<ul>";
        text += "<li>jmt</li>";
        text += "<li>...</li>";
        text += "</ul>";
        text += "<h3>Basics</h3>";
        text += "<p>";
        text += "<ul>";
        text += "<li>H   :   make this help appear</li>";
        text += "<li>Ctrl + mouse right button double click   :   choose background color</li>";
        text += "<li>Ctrl + T   :   change window title</li>";
        text += "</ul>";
        return text;
    }

    void updateTitle( QString text ) {
        this->setWindowTitle( text );
        emit windowTitleUpdated(text);
    }

    void keyPressEvent( QKeyEvent * event ) {
        if( event->key() == Qt::Key_H ) {
            help();
        }
        else if( event->key() == Qt::Key_T ) {
            if( event->modifiers() & Qt::CTRL ) {
                bool ok;
                QString text = QInputDialog::getText(this, tr(""), tr("title:"), QLineEdit::Normal,this->windowTitle(), &ok);
                if (ok && !text.isEmpty()) {
                    updateTitle(text);
                }
            }
        }
        else if(event->key() == Qt::Key::Key_C) {
            mesh.drawFace();
        }
        else if(event->key() == Qt::Key::Key_O) {
            coutPatternsStructures();
        }
    }

    void mouseDoubleClickEvent( QMouseEvent * e )
    {
        if( (e->modifiers() & Qt::ControlModifier)  &&  (e->button() == Qt::RightButton) )
        {
            pickBackgroundColor();
            return;
        }

        if( (e->modifiers() & Qt::ControlModifier)  &&  (e->button() == Qt::LeftButton) ) {
            int selectedFace = pickedGeometryIndex(e->x() , this->height() - e->y());
            if (selectedFace > -1) {
                std::cout << "selectedFace : " << selectedFace << std::endl;
                return;
            }
            else {
                showControls();
                return;
            }
        }

        QGLViewer::mouseDoubleClickEvent( e );
    }

    void mousePressEvent(QMouseEvent* e ) {
        QGLViewer::mousePressEvent(e);
    }

    void mouseMoveEvent(QMouseEvent* e  ){
        QGLViewer::mouseMoveEvent(e);
    }

    void mouseReleaseEvent(QMouseEvent* e  ) {
        QGLViewer::mouseReleaseEvent(e);
    }

signals:
    void windowTitleUpdated( const QString & );

public slots:
    void open_mesh() {
        stopAnimation();
        bool success = false;
        QString fileName = QFileDialog::getOpenFileName(nullptr,"","");
        if ( !fileName.isNull() ) { // got a file name
            if( fileName.endsWith("obj") )
                success = mesh.loadFromObj(fileName.toStdString());
            if( fileName.endsWith("ply") )
                std::cout << "THE PROGRAM DOES NOT HANDLE ply MODELS, BECAUSE ply ENCODE TRIANGLE SOUPS ONLY" << std::endl;

            if(success) {
                std::cout << fileName.toStdString() << " was opened successfully" << std::endl;
                glm::vec3 bb(FLT_MAX,FLT_MAX,FLT_MAX), BB(-FLT_MAX,-FLT_MAX,-FLT_MAX);
                //point3d bb(FLT_MAX,FLT_MAX,FLT_MAX) , BB(-FLT_MAX,-FLT_MAX,-FLT_MAX);
                for(auto v : mesh.vertexIterator()) {
                    auto coord = v->coordinate();
                    glm::vec3 vec = glm::vec3(coord.x(), coord.y(), coord.z());
                    bb = glm::min(bb, vec);
                    BB = glm::max(BB, vec);
                }
                mesh.recalculateIds();
                mesh.initializeWfcFaces();
                adjustCamera(bb, BB);
                update();
                std::cout << "Mesh bounding box : [" << bb[0] << "," << bb[1] << "," << bb[2] << "  x  " << BB[0] << "," << BB[1] << "," << BB[2] << "]" << std::endl;
            } else {
                std::cout << fileName.toStdString() << " could not be opened" << std::endl;
            }
        }
        startAnimation();
    }

    void save_mesh() {
        bool success = false;
        QString fileName = QFileDialog::getOpenFileName(nullptr,"","");
        if ( !fileName.isNull() ) { // got a file name
            success = mesh.saveOnObj(fileName.toStdString());
            if(success)
                std::cout << fileName.toStdString() << " was saved" << std::endl;
            else
                std::cout << fileName.toStdString() << " could not be saved" << std::endl;
        }
    }

    void coutPatternsStructures(){
        std::vector<std::shared_ptr<WFCPattern>> const & ps = mesh.getPatterns();
        for( int i = 0 ; i < ps.size() ; ++i ) {
            std::shared_ptr<WFCPattern> const & p = ps[i];
            std::cout << p->getName() << std::endl;
            for( int j = 0 ; j < 3 ; ++j ) {
                std::cout << "  list of adjacent patterns across edge " << j << ":" << std::endl;
                const std::unordered_map<std::shared_ptr<WFCPattern>, std::vector<int>> & adjPs = p->getNeighboursAcrossEdge(j);
                for( std::unordered_map<std::shared_ptr<WFCPattern>, std::vector<int>>::const_iterator adjP = adjPs.begin() ; adjP != adjPs.end() ; ++adjP ) {
                    std::cout << "     " << adjP->first->getName() << " has the following edges as neighbors:" << std::endl;
                    std::vector< int > const & es = adjP->second;
                    for( auto e : es ) {
                        std::cout << "       " << e << std::endl;
                    }
                }
            }
            std::cout << std::endl;
        }
    }

    void showControls()
    {
        // Show controls :
        controls->close();
        controls->show();
    }

    void drawGeometryIndices()
    {
        int redShift = 16;
        int greenShift = 8;
        int blueShift = 0;
        GLuint redMask = 0xFF << redShift;
        GLuint greenMask = 0xFF << greenShift;
        GLuint blueMask = 0xFF;

        for(auto f : mesh.faceIterator()) {
            {
                glBegin(GL_TRIANGLES);
                int index_to_draw = f->id() + 1;
                glColor4ub((index_to_draw & redMask) >> redShift, (index_to_draw & greenMask) >> greenShift, (index_to_draw & blueMask) >> blueShift, 255);
                glVertex3f(f->vertex1()->coordinate().x(), f->vertex1()->coordinate().y(), f->vertex1()->coordinate().z());
                glVertex3f(f->vertex2()->coordinate().x(), f->vertex2()->coordinate().y(), f->vertex2()->coordinate().z());
                glVertex3f(f->vertex3()->coordinate().x(), f->vertex3()->coordinate().y(), f->vertex3()->coordinate().z());
                glEnd();
            }
        }
        glEnd();
    }





    int pickedGeometryIndex(int x, int y)
    {
        //        QGLFramebufferObject * _framebuffer = new QGLFramebufferObject(QSize(this->width() , this->height()), QGLFramebufferObject::Depth);

        //        _framebuffer->bind();

        //        glViewport(0, 0, _framebuffer->width(), _framebuffer->height());

        glPushAttrib( GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT );
        //        glPushAttrib(GL_COLOR_BUFFER_BIT | GL_VIEWPORT_BIT | GL_ENABLE_BIT);
        glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

        glDisable(GL_BLEND);
        glDisable(GL_LIGHTING);
        glShadeModel(GL_FLAT);
        glEnable(GL_DEPTH);

        float back[4];
        glGetFloatv( GL_COLOR_CLEAR_VALUE , back );

        glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        drawGeometryIndices();

        GLubyte data[4];
        glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);
        int index = ((data[0] << 16) + (data[1] << 8) + data[2]);

        // do we have geometry under the mouse or not: if it's index is FFFFFF, it means we pick up
        // the clear color.
        bool isGeometry = !(index == 0xFFFFFF);
        if (!isGeometry || index < 0)
        {
            index = -1;
        }
        glClearColor(back[0], back[1], back[2], back[3]);
        glPopAttrib();

        //        _framebuffer->release();
        //        delete _framebuffer;

        return index - 1;
    }

    void saveCameraInFile(const QString &filename){
        std::ofstream out (filename.toUtf8());
        if (!out)
            exit (EXIT_FAILURE);
        // << operator for point3 causes linking problem on windows
        out << camera()->position()[0] << " \t" << camera()->position()[1] << " \t" << camera()->position()[2] << " \t" " " <<
                                          camera()->viewDirection()[0] << " \t" << camera()->viewDirection()[1] << " \t" << camera()->viewDirection()[2] << " \t" << " " <<
                                          camera()->upVector()[0] << " \t" << camera()->upVector()[1] << " \t" <<camera()->upVector()[2] << " \t" <<" " <<
                                          camera()->fieldOfView();
        out << std::endl;

        out.close ();
    }

    void openCameraFromFile(const QString &filename){

        std::ifstream file;
        file.open(filename.toStdString().c_str());

        qglviewer::Vec pos;
        qglviewer::Vec view;
        qglviewer::Vec up;
        float fov;

        file >> (pos[0]) >> (pos[1]) >> (pos[2]) >>
                                                    (view[0]) >> (view[1]) >> (view[2]) >>
                                                                                           (up[0]) >> (up[1]) >> (up[2]) >>
                                                                                                                            fov;

        camera()->setPosition(pos);
        camera()->setViewDirection(view);
        camera()->setUpVector(up);
        camera()->setFieldOfView(fov);

        camera()->computeModelViewMatrix();
        camera()->computeProjectionMatrix();

        update();
    }


    void openCamera(){
        QString fileName = QFileDialog::getOpenFileName(NULL,"","*.cam");
        if ( !fileName.isNull() ) {                 // got a file name
            openCameraFromFile(fileName);
        }
    }
    void saveCamera(){
        QString fileName = QFileDialog::getSaveFileName(NULL,"","*.cam");
        if ( !fileName.isNull() ) {                 // got a file name
            saveCameraInFile(fileName);
        }
    }

    void saveSnapShotPlusPlus(){
        QString fileName = QFileDialog::getSaveFileName(NULL,"*.png","");
        if ( !fileName.isNull() ) {                 // got a file name
            setSnapshotFormat("PNG");
            setSnapshotQuality(100);
            saveSnapshot( fileName );
            saveCameraInFile( fileName+QString(".cam") );
        }
    }
};




#endif // MYVIEWER_H
