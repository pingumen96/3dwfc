#include "Texture.h"
#include "WFCPattern.h"

using namespace std;

WFCPattern::WFCPattern() {
}

WFCPattern::WFCPattern(const std::shared_ptr<Texture>& texture, std::string name)
    : texture(texture)
    , name(name)
    , neighbourAcrossEdge(std::vector<std::unordered_map<std::shared_ptr<WFCPattern>, std::vector<int>>>(3)) {
}


void WFCPattern::addNeighbour(unsigned short edgeNumber, std::shared_ptr<WFCPattern> patternPtr, unsigned short neighbourEdgeNumber) {
    neighbourAcrossEdge[edgeNumber][patternPtr].emplace_back(neighbourEdgeNumber);
}
