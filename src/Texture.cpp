#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include "gl/stb_image.h"
#include "Texture.h"

Texture::Texture() : id(0), index(0), width(0), height(0), nChannels(0), active(false), format(GL_RGB){

}

Texture::Texture(const std::string &filename) {
    stbi_set_flip_vertically_on_load(true);
    /*unsigned char **/data = stbi_load(filename.c_str(), &width, &height, &nChannels, 0);
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    // non sono sicuro che serva settarli ogni volta, verificare
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if(data) {
        format = GL_RGB;
        if(nChannels == 4) {
            format = GL_RGBA;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, static_cast<GLenum>(format), GL_UNSIGNED_BYTE, data);
    } else {
        std::cout << "Failed to load texture" << std::endl;
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    //stbi_image_free(data);
}

void Texture::setActive(bool value)
{
    active = value;
    if(value) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, id);
    } else {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
