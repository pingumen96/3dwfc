#include <random>
#include <vector>
#include <iterator>
#include <memory>
#include <limits>
#include "WFCMesh.h"

WFCMesh::WFCMesh() : cg3::Dcel() {
    currentFaceId = -1;
}

void WFCMesh::initializeWfcFaces() {
    // hardcoded part
    enum Patterns {ROUNDABOUT_EXIT, ROAD , ROUNDABOUT
                   , GRASS
                  };
    // patterns initialization with texture
    patterns.emplace_back(std::make_shared<WFCPattern>(std::make_shared<Texture>("./textures/roundabout_exit.png"), "roundabout_exit"));
    patterns.emplace_back(std::make_shared<WFCPattern>(std::make_shared<Texture>("./textures/road.png"), "road"));
    patterns.emplace_back(std::make_shared<WFCPattern>(std::make_shared<Texture>("./textures/roundabout.png"), "roundabout"));
    patterns.emplace_back(std::make_shared<WFCPattern>(std::make_shared<Texture>("./textures/grass.png"), "grass"));

    // adding neighbours...
    patterns[GRASS]->addNeighbour(0, patterns[ROAD], 2);
    patterns[GRASS]->addNeighbour(1, patterns[ROAD], 2);
    patterns[GRASS]->addNeighbour(2, patterns[ROAD], 2);
    patterns[GRASS]->addNeighbour(0, patterns[ROUNDABOUT], 0);
    patterns[GRASS]->addNeighbour(1, patterns[ROUNDABOUT], 0);
    patterns[GRASS]->addNeighbour(2, patterns[ROUNDABOUT], 0);
    patterns[GRASS]->addNeighbour(0, patterns[GRASS], 0);
    patterns[GRASS]->addNeighbour(1, patterns[GRASS], 0);
    patterns[GRASS]->addNeighbour(2, patterns[GRASS], 0);
    patterns[GRASS]->addNeighbour(0, patterns[GRASS], 1);
    patterns[GRASS]->addNeighbour(1, patterns[GRASS], 1);
    patterns[GRASS]->addNeighbour(2, patterns[GRASS], 1);
    patterns[GRASS]->addNeighbour(0, patterns[GRASS], 2);
    patterns[GRASS]->addNeighbour(1, patterns[GRASS], 2);
    patterns[GRASS]->addNeighbour(2, patterns[GRASS], 2);

    patterns[ROUNDABOUT_EXIT]->addNeighbour(0, patterns[ROAD], 0);
    patterns[ROUNDABOUT_EXIT]->addNeighbour(0, patterns[ROAD], 1);
    patterns[ROUNDABOUT_EXIT]->addNeighbour(1, patterns[ROUNDABOUT_EXIT], 2);
    patterns[ROUNDABOUT_EXIT]->addNeighbour(1, patterns[ROUNDABOUT], 2);
    patterns[ROUNDABOUT_EXIT]->addNeighbour(2, patterns[ROUNDABOUT_EXIT], 1);
    patterns[ROUNDABOUT_EXIT]->addNeighbour(2, patterns[ROUNDABOUT], 1);

    patterns[ROUNDABOUT]->addNeighbour(0, patterns[GRASS], 0);
    patterns[ROUNDABOUT]->addNeighbour(0, patterns[GRASS], 1);
    patterns[ROUNDABOUT]->addNeighbour(0, patterns[GRASS], 2);
    patterns[ROUNDABOUT]->addNeighbour(1, patterns[ROUNDABOUT_EXIT], 2);
    patterns[ROUNDABOUT]->addNeighbour(1, patterns[ROUNDABOUT], 2);
    patterns[ROUNDABOUT]->addNeighbour(2, patterns[ROUNDABOUT], 1);
    patterns[ROUNDABOUT]->addNeighbour(2, patterns[ROUNDABOUT_EXIT], 1);

    patterns[ROAD]->addNeighbour(0, patterns[ROUNDABOUT_EXIT], 0);
    patterns[ROAD]->addNeighbour(0, patterns[ROAD], 0);
    patterns[ROAD]->addNeighbour(0, patterns[ROAD], 1);
    patterns[ROAD]->addNeighbour(1, patterns[ROUNDABOUT_EXIT], 0);
    patterns[ROAD]->addNeighbour(1, patterns[ROAD], 0);
    patterns[ROAD]->addNeighbour(1, patterns[ROAD], 1);
    patterns[ROAD]->addNeighbour(2, patterns[GRASS], 0);
    patterns[ROAD]->addNeighbour(2, patterns[GRASS], 1);
    patterns[ROAD]->addNeighbour(2, patterns[GRASS], 2);
    patterns[ROAD]->addNeighbour(2, patterns[ROAD], 2);
    patterns[ROAD]->addNeighbour(2, patterns[ROUNDABOUT], 0);

    for(unsigned int i = 0; i < faces.size(); i++) {
        wfcFaces.insert(std::make_pair<unsigned int, WFCFace>(faces[i]->id(), WFCFace(nullptr, i, patterns)));
    }

    // tests
    //wfcFaces[1].setPattern(patterns["road"], 1);
}





bool WFCMesh::isPatternValid( long f1 , std::shared_ptr<WFCPattern> p1 , short r1 ) {
    auto it = *(faces[f1]->incidentHalfEdgeIterator().begin());
    for(short eIt1 = 0; eIt1 < 3; eIt1++) {
        auto f2 = it->twin()->face()->id();

        int eIt2 = -1;
        auto it2 = *(faces[f2]->incidentHalfEdgeIterator().begin());
        for(short j = 0; j < 3; j++) {
            auto neighborFaceIdx2 = it2->twin()->face()->id();
            if(neighborFaceIdx2 == f1) {
                eIt2 = j;
            }
            it2 = it2->next();
        }

        if( wfcFaces[faces[f2]->id()].isAssigned() ) {
            int p1EdgeIt = ( (eIt1 + 3) - r1  ) % 3;
            int r2 = wfcFaces[faces[f2]->id()].getNumRotations();
            int p2EdgeIt = ( (eIt2 + 3) - r2  ) % 3;
            std::shared_ptr<WFCPattern> p2 = wfcFaces[faces[f2]->id()].getPattern();

            if( ! p1->isOtherPatternAPossibleNeighbor(p1EdgeIt , p2 , p2EdgeIt) )
                return false;
        }

        it = it->next();
    }
    return true;
}





void WFCMesh::drawFace(){
    // random
    std::random_device dev;
    std::mt19937 rng(dev());

    if(currentFaceId == -1) {
        // random face
        std::uniform_int_distribution<std::mt19937::result_type> dist(0, faces.size() - 1);
        currentFaceId = dist(rng);

        // a random pattern is chosen
        std::uniform_int_distribution<std::mt19937::result_type> distPattern(0, patterns.size() - 1);
        unsigned int patternIndex = distPattern(rng);

        wfcFaces[currentFaceId].setPattern(patterns[patternIndex] , 0);
    } else {
        // inizio parte sicuramente corretta, si calcola quali possono essere i vicini e con quale rotazione
        int counter = 0;
        long nextFaceId;
        auto he = *(faces[wfcFaces[currentFaceId].getCorrespondingFaceIndex()]->incidentHalfEdgeIterator().begin());
        for(int i = 0; i < 3; i++) {
            if(!wfcFaces[he->twin()->face()->id()].isAssigned()) {
                nextFaceId = he->twin()->face()->id();
                break;
            }
            counter++;
            he = he->next();
        }

        if(counter == 3) return;

        std::cout << "Looking at face " << currentFaceId << " with pattern " << wfcFaces[currentFaceId].getPattern()->getName() <<
                     " and rotation " << wfcFaces[currentFaceId].getNumRotations() << std::endl;
        std::cout << "Considering neighbor " << nextFaceId << " across edge nb " << counter << std::endl;

        bool hasFoundValidPattern = false;
        for ( auto p : patterns ) { // iterate over all patterns
            for ( short r = 0 ; r < 3 ; ++r ) { // iterate over all rotations
                if( isPatternValid( nextFaceId , p , r ) ) {
                    std::cout << "We have found pattern " << p->getName() << " with rotation " << r << std::endl << std::endl;
                    wfcFaces[nextFaceId].setPattern(p, r);
                    hasFoundValidPattern = true;
                    break;
                }
            }
            if(hasFoundValidPattern)
                break;
        }

        currentFaceId = nextFaceId;
    }
}
