#ifndef WFCPATTERN_H
#define WFCPATTERN_H
#include <memory>
#include <vector>
#include <string>
#include <unordered_map>
#include "Texture.h"

// deve esistere un solo esemplare di ciascun pattern,
// si userà il puntatore all'esemplare
class WFCPattern {
public:
    WFCPattern();
    WFCPattern(const std::shared_ptr<Texture>& texture, std::string name);

    void addNeighbour(unsigned short edgeNumber, std::shared_ptr<WFCPattern> patternPtr, unsigned short neighbourEdgeNumber);
    const std::shared_ptr<Texture>& getTexture() {return texture;}
    const std::unordered_map<std::shared_ptr<WFCPattern>, std::vector<int>> & getNeighboursAcrossEdge(unsigned int i) {return neighbourAcrossEdge[i];}

    bool isOtherPatternAPossibleNeighbor( int edgeIt , std::shared_ptr<WFCPattern> otherPattern , int otherEdge ) {
        if( neighbourAcrossEdge[edgeIt].find( otherPattern  )  ==  neighbourAcrossEdge[edgeIt].end() )
            return false;
        for (auto e : neighbourAcrossEdge[edgeIt][otherPattern]) {
            if(e == otherEdge) return true;
        }
        return false;
    }

    std::string getName() const {return name;}
private:
    std::shared_ptr<Texture> texture;

    std::string name;

    // alla faccia i ([]) ci si può connettere con la faccia j (int) del pattern dato (WFCPattern)
    std::vector<std::unordered_map<std::shared_ptr<WFCPattern>, std::vector<int>>> neighbourAcrossEdge;
};

#endif // WFCPATTERN_H
