#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/gl.h>

struct Texture {
    Texture();
    Texture(const std::string& filename);
    GLuint id;
    unsigned int index;
    int width;
    int height;
    int nChannels;
    GLint format;
    unsigned char* data;
    bool active;
    void setActive(bool value);
};

#endif // TEXTURE_H
